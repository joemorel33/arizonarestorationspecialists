
var $j = jQuery.noConflict();

$j(document).ready(function() {
	"use strict";

	});

  var $_GET=[];
  window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(a,name,value){$_GET[name]=value;});

  var city = window.decodeURI($_GET["city"]).replace(/\+/g, " ");
  var kw = window.decodeURI($_GET["kw"]).replace(/\+/g, " ");

  if (city !== "undefined") {
    sessionStorage.setItem("city", city);
  } else {
    city = sessionStorage.getItem("city");
  }
  if (kw !== "undefined") {
    sessionStorage.setItem("kw", kw);
  } else {
    kw = sessionStorage.getItem("kw");
  }

  if (city === "undefined") city = "Emergency";
  if (kw === "undefined") kw = "Water Damage";

  if (city && city !== undefined && city.length>1) {
    // All words
    var elements = document.getElementsByClassName("replace-city");
    for (var x=0; x<elements.length; x++) {
      elements[x].innerHTML = city;
    }
    // Update page title
    document.title = document.title.replace("Water Damage", city);
  }

  if (kw && kw !== undefined && kw.length>1) {
    var elements = document.getElementsByClassName("replace-kw");
    for (var x=0; x<elements.length; x++) {
      elements[x].innerHTML = kw;
    }
    // Update page title
    document.title = document.title.replace("Water Damage", kw);
    document.title = document.title.replace(city, city+" "+kw);
  }


$j(document).ready(function () {
    size_li = $j("#quote-list li").size();
    x=8;
    
    if (location.href.indexOf('contact-us') != -1) {
        $j('#quote-list li').show();
        //$j('#quote-list').bxSlider();
        return;
    }
    
    $j('#quote-list li:lt('+x+')').show();
    $j('#loadMore').click(function () {
        x= (x+10 <= size_li) ? x+10 : size_li;
        $j('#quote-list li:lt('+x+')').show();
    });
    $j('#showLess').click(function () {
        x=(x-10<0) ? 8 : x-10;
        $j('#quote-list li').not(':lt('+x+')').hide();
    });
});

$j(window).scroll(function(){
  if($j(window).scrollTop() > 200){
      $j("#sticky-phone").show();
  }
});
$j(window).scroll(function(){
  if($j(window).scrollTop() < 190){
      $j("#sticky-phone").hide();
  }
});