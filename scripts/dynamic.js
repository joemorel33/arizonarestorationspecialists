// v2
(function () {
	function setCookie(c_name,value,expiredays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate()+expiredays);
		document.cookie=c_name+ "=" +escape(value)+
		((expiredays==null) ? "" : ";expires="+exdate.toUTCString())+"; path=/";
	}
	
	function getCookie(c_name)
	{
		if (document.cookie.length>0)
		  {
		  c_start=document.cookie.indexOf(c_name + "=");
		  if (c_start!=-1)
		    {
		    c_start=c_start + c_name.length+1;
		    c_end=document.cookie.indexOf(";",c_start);
		    if (c_end==-1) c_end=document.cookie.length;
		    return unescape(document.cookie.substring(c_start,c_end));
		    }
		  }
		return "";
	}

	function parseQuery(url) {
	    var query = {};
	    
	    if (url.indexOf('?') == -1) {
		    return {};
	    }
	    qstr = url.substr(url.indexOf('?'));
	    
	    var a = qstr.substr(1).split('&');
	    for (var i = 0; i < a.length; i++) {
	      var b = a[i].split('=');
	        query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	    }
	    return query;
	}
	
	var q = parseQuery(location.href);

	// HIDE	
	if (q.hide && q.hide.match(/^off$/i)) {
		setCookie('hide', '', -1);
		delete q.hide;
	}
	
	if ((q.hide && q.hide.match(/^on$/i))  || getCookie('hide') != '') {
		setCookie('hide', 1, 10);
		
		var els = document.getElementsByClassName("hide");
		for (var x = 0; x < els.length; x++) {
//			els[x].parentNode.removeChild(els[x]);
			els[x].style.display = 'none';
		}
		
		var els = document.getElementsByClassName("hide33");
		for (var x = 0; x < els.length; x++) {
// 			els[x].parentNode.removeChild(els[x]);
			els[x].style.display = 'none';
		}
	}
	
	// REPLACE
	var replaces = {};
	if (Storage && localStorage.replaces) {
		try {
			replaces = JSON.parse(localStorage.replaces);
		} catch (e) {
			console.log("Failed to parse replaces json");
		}
	}
	
	for (var x in q) {
		if (x.indexOf('replace') === 0) {
			replaces[x] = q[x];		
		}
	}
	
	for (var cl in replaces) {
		var els = document.getElementsByClassName(cl);
		for (var x = 0; x <= els.length; x++) {
			if (els[x]) {
				els[x].innerText = replaces[cl].replace(/\+/g, ' ');
			}
		}
	}
	
	localStorage.replaces = JSON.stringify(replaces);
var urlParams = new URLSearchParams(window.location.search);
     var imageURL = '/images/' + urlParams.get('replace') + '.jpg';
     jQuery(".replaceImage").css("background","url('" + imageURL + "') no-repeat center center");
})();