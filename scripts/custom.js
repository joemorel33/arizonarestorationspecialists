jQuery(window).scroll( function () {
    $=jQuery;
    var isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;
    var triggerTop = ($(".red-search").length>0 ? $(".red-search").offset().top : 300);
    if (isMobile) {
        if ($(window).scrollTop() >= ($(".head-holder .red-search").height() + triggerTop - $(".header_bottom").height() + 100)) {
            $("#mobile-phone").css("opacity", 1);
            $("img.always").css("opacity", 0);
        } else {
            $("#mobile-phone").css("opacity", 0);
            $("img.always").css("opacity", 1);
        }
    }
} );