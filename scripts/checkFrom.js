function  isNumber(String)
		{  
			var  Letters  =  "1234567890";
			var  i;
			var  c;
			if(String.charAt(  0  )=='-')
			return  false;
			if(  String.charAt(  String.length  -  1  )  ==  '-'  )
			return  false;
			for(  i  =  0;  i < String.length;  i++  )
			{  
			c  =  String.charAt(  i  );
			if  (Letters.indexOf(  c  ) <  0)
			return  false;
			}
			return  true;
		}

		function chkElement(obj,ctxt,stus){
			var chkValue;
			chkValue = document.getElementById(obj);
			if( chkValue.value!=ctxt &&  chkValue.value != ""){
				chkValue.className="";
				return true;
			}else{
				if( chkValue.value ==""){
					chkValue.value=ctxt;
					chkValue.className="orl";
				}else if(!stus){
					chkValue.value="";
					chkValue.className="";
				}
				return false;
			}
		}
		
		function chkform(){
			var	error=false;
			var err="";
			if(!chkElement('zipcode','ENTER ZIP Code...',1)){
				err+="Please input Zip code.\n";
				error=true;
			}else if(!isNumber(document.getElementById('zipcode').value)  || document.getElementById('zipcode').value.length != 5){
				err+="Please input valid Zip code.\n";
				error=true;
			}
 
 			if(error){
				alert(err);
				return false;
			}
		
		}
		
