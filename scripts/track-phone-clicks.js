(function () {
	$ = jQuery;

	var links = document.links;

	for ( var i = 0; i < links.length; i++ ) {
		if (links[i] == "function") {
		    continue;
		}

		var l = links[i];

		$(l).click(function () {
			var linkHref = this.href;

			console.log(linkHref.match(/tel:/i));
			if ( linkHref.match(/tel:/i) ) {
				ga('send', 'event', 'Contact', 'Phone', linkHref.replace(/\D/g,''));
			}
		});
	}
})();