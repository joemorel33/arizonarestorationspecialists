// Master phone.js file version 2.0 for restorationlocal.com waterdamage.restorationlocal.com 247waterrestoration.com waterdamage.247waterrestoration.com
// Last updated:  2017-10-12 by Joe Morel
// Refreshed phone list


var updatePhoneNumber = function () {
    var phones = [
['lrr_crawl_space','501-214-5432'],
['lrr_crawl_space_city','501-943-5018'],
['lrr_flood_cleanup','501-214-7135'],
['lrr_flood_cleanup_city','501-504-7197'],
['lrr_flood_restoration','501-302-4631'],
['lrr_flood_restoration_city','501-214-6430'],
['lrr_hot_water_tank','501-238-8018'],
['lrr_hot_water_tank_city','501-214-6113'],
['lrr_restoration_compaines','501-267-8122'],
['lrr_restoration_compaines_city','501-214-5794'],
['lrr_water_cleanup','501-214-5391'],
['lrr_water_cleanup_city','501-267-8041'],
['lrr_water_damage','501-214-5293'],
['lrr_water_damage_city','501-214-6285'],
['lrr_water_removal','501-482-5172'],
['lrr_water_removal_city','501-214-5395'],
['lrr_water_restoration','501-214-6045'],
['lrr_water_restoration_city','501-214-6413'],
['lrr_fire_cleanup','501-625-9206'],
['lrr_fire_damage','501-214-7152'],
['lrr_fire_restoration','501-242-5952'],
['lrr_smoke_damage','501-214-6214'],
['lrr_smoke_removal','501-214-6262'],
['lrr_smoke_restoration','501-214-7182'],
['lrr_soot_cleanup','501-238-8486']
    ];

    var urlPhones = [
        ['ceiling-water-damage-leak','888-915-7176'],
        ['the-dangers-of-stagnant-water','888-514-2501'],
        ['water-damage-toilet-overflow','888-787-0898'],
        ['what-is-the-average-cost-of-water-mitigation-service','888-925-0250'],
        ['fixing-water-damage-cabinets','888-609-1520']
    ];


    var blacklist = ['888-594-8381', '888-222-2222'];

    function formatPhoneNumber( num ) {
        num = num.toString();
        num = num.replace( /\D/g, '' );

        if ( num.substring( 0, 1 ) == 1 ) {
            num = num.slice( 1 );
        }

        if ( [ '800', '888', '877', '866', '855', '844' ].indexOf( num.substring( 0, 3 ) ) === -1 ) {
            return '(' + num.substr( 0, 3 ) + ') ' + num.substr( 3, 3 ) + '-' + num.substr( 6, 4 );
        } else {
            return '1-' + num.substr( 0, 3 ) + '-' + num.substr( 3, 3 ) + '-' + num.substr( 6, 4 );
        }
    }

    function formatPhoneDashes( num ) {
        num = num.toString();
        num = num.replace( /\D/g, '' );

        if ( num.substring( 0, 1 ) == 1 ) {
            num = num.slice( 1 );
        }

        return '1-' + num.substr( 0, 3 ) + '-' + num.substr( 3, 3 ) + '-' + num.substr( 6, 4 );
    }

    function setCookie(c_name,value,expiredays)
    {
        var exdate=new Date();
        exdate.setDate(exdate.getDate()+expiredays);
        document.cookie=c_name+ "=" +escape(value)+
            ((expiredays==null) ? "" : ";expires="+exdate.toUTCString())+"; path=/";
    }

    function getCookie(c_name)
    {
        if (document.cookie.length>0)
        {
            c_start=document.cookie.indexOf(c_name + "=");
            if (c_start!=-1)
            {
                c_start=c_start + c_name.length+1;
                c_end=document.cookie.indexOf(";",c_start);
                if (c_end==-1) c_end=document.cookie.length;
                return unescape(document.cookie.substring(c_start,c_end));
            }
        }
        return "";
    }

    function parseQuery(url) {
        var query = {};

        if (url.indexOf('?') == -1) {
            return {};
        }
        qstr = url.substr(url.indexOf('?'));

        var a = qstr.substr(1).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
    }

    function parseDomain(url) {
        var domain;
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];

        return domain;
    }

    if (getCookie("phonereferrer") == '') {
        refQuery = parseQuery(document.referrer);
        setCookie('phonereferrer', document.referrer, 10);
    } else {
        refQuery = parseQuery(getCookie("phonereferrer"));
    }

    query = parseQuery(location.href);

    for (var x in phones) {
        phones[x][0] = phones[x][0].toLowerCase();

        if (parseDomain(document.referrer) == phones[x][0]) {
            setCookie('phonereferrer', document.referrer, 10); // refDomain + '|' +
        }

        for (var y in query) {
            if (y.toLowerCase() == phones[x][0]) {
                setCookie('phonereferrer', location.href, 10); // refDomain + '|' +
                break;
            }
        }
    }

    var finalphone;


//Adding code to support Visual Web Optimizer

    outer_loop:
        for (var x in phones) {
            for (var y in query) {
                if(jQuery("#VWO").attr("value") || query["vwo"]) { y += "B"; }
                console.log("Query: " + y);
                if (y.toLowerCase() == phones[x][0]) {
                    finalphone = phones[x][1];
                    break outer_loop;
                }
            }
        }

    if (!finalphone) {
        var d = parseDomain(getCookie("phonereferrer"));
        if(jQuery("#VWO").attr("value") || query["vwo"]) { d += "B"; }
        for (var x in phones) {
            if (d == phones[x][0]) {
                finalphone = phones[x][1];
                break;
            }
        }
    }

    if (!finalphone) {
        ref_outer_loop:
            for (var x in phones) {
                for (var y in refQuery) {
                    if(jQuery("#VWO").attr("value") || query["vwo"]) { y += "B"; }
                    if (y.toLowerCase() == phones[x][0]) {
                        finalphone = phones[x][1];
                        break ref_outer_loop;
                    }
                }
            }
    }

    if(!finalphone) {
        outer_loop2:
            for (var x in phones) {
                for (var y in query) {
                    if(jQuery("#VWO").attr("value") || query["vwo"]) { y += "B"; }
                    if (y.toLowerCase() == phones[x][0]) {
                        finalphone = phones[x][1];
                        break outer_loop2;
                    }
                }
            }
    }

    if (!finalphone) {
        var d = parseDomain(getCookie("phonereferrer"));
        for (var x in phones) {
            if (d == phones[x][0]) {
                finalphone = phones[x][1];
                break;
            }
        }
    }

    if (!finalphone) {
        ref_outer_loop2:
            for (var x in phones) {
                for (var y in refQuery) {
                    if (y.toLowerCase() == phones[x][0]) {
                        finalphone = phones[x][1];
                        break ref_outer_loop2;
                    }
                }
            }
    }

    //Adding code to look at URL slug
    if (!finalphone) {
      console.log('Looking for URL slug');
      for(var urlPhone in urlPhones) {
        console.log('Looking for ' + urlPhones[urlPhone][0]);
        if(document.location.href.includes(urlPhones[urlPhone][0]))
          finalphone = urlPhones[urlPhone][1];
      }
    }

    function updatePhone() {
        if (getCookie("phonereferrer") == '') {
            refQuery = parseQuery(document.referrer);
            setCookie('phonereferrer', document.referrer, 10);
        } else {
            refQuery = parseQuery(getCookie("phonereferrer"));
        }

        query = parseQuery(location.href);

        for (var x in phones) {
            phones[x][0] = phones[x][0].toLowerCase();

            if (parseDomain(document.referrer) == phones[x][0]) {
                setCookie('phonereferrer', document.referrer, 10); // refDomain + '|' +
            }

            for (var y in query) {
                if (y.toLowerCase() == phones[x][0]) {
                    setCookie('phonereferrer', location.href, 10); // refDomain + '|' +
                    break;
                }
            }
        }

        var finalphone;

        outer_loop:
            for (var x in phones) {
                for (var y in query) {
                    if(jQuery("#VWO").attr("value") || query["B"]) { y += "B"; console.log("Found B version");}
                    console.log("Query: " + y);
                    if (y.toLowerCase() == phones[x][0]) {
                        finalphone = phones[x][1];
                        break outer_loop;
                    }
                }
            }

        if (!finalphone) {
            var d = parseDomain(getCookie("phonereferrer"));
            if(jQuery("#VWO").attr("value") || query["B"]) { d += "B"; }
            for (var x in phones) {
                if (d == phones[x][0]) {
                    finalphone = phones[x][1];
                    break;
                }
            }
        }

        if (!finalphone) {
            ref_outer_loop:
                for (var x in phones) {
                    for (var y in refQuery) {
                        if(jQuery("#VWO").attr("value") || query["B"]) { y += "B"; }
                        if (y.toLowerCase() == phones[x][0]) {
                            finalphone = phones[x][1];
                            break ref_outer_loop;
                        }
                    }
                }
        }

        if(!finalphone) {
            outer_loop2:
                for (var x in phones) {
                    for (var y in query) {
                        if(jQuery("#VWO").attr("value") || query["B"]) { y += "B"; }
                        if (y.toLowerCase() == phones[x][0]) {
                            finalphone = phones[x][1];
                            break outer_loop2;
                        }
                    }
                }
        }

        if (!finalphone) {
            var d = parseDomain(getCookie("phonereferrer"));
            for (var x in phones) {
                if (d == phones[x][0]) {
                    finalphone = phones[x][1];
                    break;
                }
            }
        }

        if (!finalphone) {
            ref_outer_loop2:
                for (var x in phones) {
                    for (var y in refQuery) {
                        if (y.toLowerCase() == phones[x][0]) {
                            finalphone = phones[x][1];
                            break ref_outer_loop2;
                        }
                    }
                }
        }
    }

    window.findAndReplaceDOMText = (function() {
        /**
         * findAndReplaceDOMText
         *
         * Locates matches and replaces with replacementNode
         *
         * @param {RegExp} regex The regular expression to match
         * @param {Node} node Element or Text node to search within
         * @param {String|Element|Function} replacementNode A NodeName,
         *  Node to clone, or a function which returns a node to use
         *  as the replacement node.
         * @param {Number} captureGroup A number specifiying which capture
         *  group to use in the match. (optional)
         */
        function findAndReplaceDOMText(regex, node, replacementNode, captureGroup) {

            var m, matches = [], text = _getText(node);
            var replaceFn = _genReplacer(replacementNode);

            if (!text) { return; }

            if (regex.global) {
                while (m = regex.exec(text)) {
                    matches.push(_getMatchIndexes(m, captureGroup));
                }
            } else {
                m = text.match(regex);
                matches.push(_getMatchIndexes(m, captureGroup));
            }

            if (matches.length) {
                _stepThroughMatches(node, matches, replaceFn);
            }
        }

        /**
         * Gets the start and end indexes of a match
         */
        function _getMatchIndexes(m, captureGroup) {

            captureGroup = captureGroup || 0;

            if (!m[0]) throw 'findAndReplaceDOMText cannot handle zero-length matches';

            var index = m.index;

            if (captureGroup > 0) {
                var cg = m[captureGroup];
                if (!cg) throw 'Invalid capture group';
                index += m[0].indexOf(cg);
                m[0] = cg;
            }

            return [ index, index + m[0].length, [ m[0] ] ];
        };

        /**
         * Gets aggregate text of a node without resorting
         * to broken innerText/textContent
         */
        function _getText(node) {

            if (node.nodeType === 3) {
                return node.data;
            }

            var txt = '';

            if (node = node.firstChild) do {
                txt += _getText(node);
            } while (node = node.nextSibling);

            return txt;

        }

        /**
         * Steps through the target node, looking for matches, and
         * calling replaceFn when a match is found.
         */
        function _stepThroughMatches(node, matches, replaceFn) {

            var after, before,
                startNode,
                endNode,
                startNodeIndex,
                endNodeIndex,
                innerNodes = [],
                atIndex = 0,
                curNode = node,
                matchLocation = matches.shift(),
                matchIndex = 0;

            out: while (true) {

                if (curNode.nodeType === 3) {
                    if (!endNode && curNode.length + atIndex >= matchLocation[1]) {
                        // We've found the ending
                        endNode = curNode;
                        endNodeIndex = matchLocation[1] - atIndex;
                    } else if (startNode) {
                        // Intersecting node
                        innerNodes.push(curNode);
                    }
                    if (!startNode && curNode.length + atIndex > matchLocation[0]) {
                        // We've found the match start
                        startNode = curNode;
                        startNodeIndex = matchLocation[0] - atIndex;
                    }
                    atIndex += curNode.length;
                }

                if (startNode && endNode) {
                    curNode = replaceFn({
                        startNode: startNode,
                        startNodeIndex: startNodeIndex,
                        endNode: endNode,
                        endNodeIndex: endNodeIndex,
                        innerNodes: innerNodes,
                        match: matchLocation[2],
                        matchIndex: matchIndex
                    });



                    // replaceFn has to return the node that replaced the endNode
                    // and then we step back so we can continue from the end of the
                    // match:
                    atIndex -= (endNode.length - endNodeIndex);
                    startNode = null;
                    endNode = null;
                    innerNodes = [];
                    matchLocation = matches.shift();
                    matchIndex++;
                    if (!matchLocation) {
                        break; // no more matches
                    }
                } else if (curNode.firstChild || curNode.nextSibling) {
                    // Move down or forward:
                    curNode = curNode.firstChild || curNode.nextSibling;
                    continue;
                }

                // Move forward or up:
                while (true) {
                    if (curNode == undefined) {
                        console.log("phone.js:210 - curNode is undefined");
//        return;
                    }
                    if (curNode != undefined && curNode.nextSibling) {
                        curNode = curNode.nextSibling;
                        break;
                    } else if (curNode != undefined && curNode.parentNode !== node) {
                        curNode = curNode.parentNode;
                    } else {
                        break out;
                    }
                }

            }

        }

        var reverts;
        /**
         * Reverts the last findAndReplaceDOMText process
         */
        findAndReplaceDOMText.revert = function revert() {
            for (var i = 0, l = reverts.length; i < l; ++i) {
                reverts[i]();
            }
            reverts = [];
        };

        /**
         Finds parent node with a phone link
         */
        function findAncestorPhoneLink (el) {
            while (el = el.parentElement) {
                if (el.tagName == 'A') {
                    if (el.getAttribute("href").indexOf('tel:') != -1) {
                        return el;
                    }
                }
            }
            return false;
        }


        /**
         * Generates the actual replaceFn which splits up text nodes
         * and inserts the replacement element.
         */
        function _genReplacer(nodeName) {

            reverts = [];

            var makeReplacementNode;

            if (typeof nodeName != 'function') {
                var stencilNode = nodeName.nodeType ? nodeName : document.createElement(nodeName);
                makeReplacementNode = function(fill) {
                    var clone = document.createElement('div'),
                        el;
                    clone.innerHTML = stencilNode.outerHTML || new XMLSerializer().serializeToString(stencilNode);
                    el = clone.firstChild;
                    if (fill) {
                        el.appendChild(document.createTextNode(fill));
                    }
                    return el;
                };
            } else {
                makeReplacementNode = nodeName;
            }

            return function replace(range) {

                var startNode = range.startNode,
                    endNode = range.endNode,
                    matchIndex = range.matchIndex;

                if (startNode === endNode) {
                    var node = startNode;
                    if (range.startNodeIndex > 0) {
                        // Add `before` text node (before the match)
                        var before = document.createTextNode(node.data.substring(0, range.startNodeIndex));
                        node.parentNode.insertBefore(before, node);
                    }

                    // Create the replacement node:
                    var el = makeReplacementNode(range.match[0], matchIndex, findAncestorPhoneLink(node));

                    if (el == false) {
                        return;
                    }

                    node.parentNode.insertBefore(el, node);
                    if (range.endNodeIndex < node.length) {
                        // Add `after` text node (after the match)
                        var after = document.createTextNode(node.data.substring(range.endNodeIndex));
                        node.parentNode.insertBefore(after, node);
                    }
                    node.parentNode.removeChild(node);
                    reverts.push(function() {
                        var pnode = el.parentNode;
                        pnode.insertBefore(el.firstChild, el);
                        pnode.removeChild(el);
                        pnode.normalize();
                    });
                    return el;
                } else {
                    // Replace startNode -> [innerNodes...] -> endNode (in that order)
                    var before = document.createTextNode(startNode.data.substring(0, range.startNodeIndex));
                    var after = document.createTextNode(endNode.data.substring(range.endNodeIndex));
                    var elA = makeReplacementNode(startNode.data.substring(range.startNodeIndex), matchIndex);

                    var innerEls = [];
                    for (var i = 0, l = range.innerNodes.length; i < l; ++i) {
                        var innerNode = range.innerNodes[i];
                        var innerEl = makeReplacementNode(innerNode.data, matchIndex);
                        innerNode.parentNode.replaceChild(innerEl, innerNode);
                        innerEls.push(innerEl);
                    }
                    var elB = makeReplacementNode(endNode.data.substring(0, range.endNodeIndex), matchIndex);
                    startNode.parentNode.insertBefore(before, startNode);
                    startNode.parentNode.insertBefore(elA, startNode);
                    startNode.parentNode.removeChild(startNode);
                    endNode.parentNode.insertBefore(elB, endNode);
                    endNode.parentNode.insertBefore(after, endNode);
                    endNode.parentNode.removeChild(endNode);
                    reverts.push(function() {
                        innerEls.unshift(elA);
                        innerEls.push(elB);
                        for (var i = 0, l = innerEls.length; i < l; ++i) {
                            var el = innerEls[i];
                            var pnode = el.parentNode;
                            pnode.insertBefore(el.firstChild, el);
                            pnode.removeChild(el);
                            pnode.normalize();
                        }
                    });
                    return elB;
                }
            };

        }

        return findAndReplaceDOMText;

    }());

    var negativeRegex = /\d{12}/i;
    var linkRegex = /1?-?\d{3}-?\d{3}-?\d{4}/gi;
    var textRegex = /(1[ -]?)?((((\(\d{3}\)) ?|(\d{3}-))\d{3}-\d{4}))/gi;


// Loop through and replace tel: attributes
    var links = document.links;

    if (finalphone) {
        for ( var i = 0; i < links.length; i++ ) {
            if (links[i] == "function") {
                continue;
            }

            var linkHref = links[ i ].href;
            var match = linkHref.match( linkRegex );

            if ( !linkHref.match(negativeRegex) && linkHref.match(/tel/i) && match ) {
                var blacklisted = 0;
                for (var x in blacklist) {
                    if (formatPhoneNumber(blacklist[x]) == formatPhoneNumber(match[0])) {
                        blacklisted = 1;
                        break;
                    }
                }

                if (!blacklisted) {
                    links[ i ].setAttribute( 'href', linkHref.replace(linkRegex, formatPhoneDashes( finalphone ) ));
                }
            }
        }
    }

    var container = document.body;
    if (finalphone != undefined) {
        findAndReplaceDOMText(/(1[ -]?)?((((\(\d{3}\)) ?|(\d{3}-))\d{3}-\d{4}))/gi, container, function(fill, matchIndex, parentLink) {
            // fill.replace(/[^0-9]/g, "");

            for (var x in blacklist) {
                if (formatPhoneNumber(blacklist[x]) == formatPhoneNumber(fill)) {
                    return document.createTextNode(fill);
                }
            }

            if (parentLink) {
                parentLink.setAttribute('href', 'tel:' + formatPhoneDashes(finalphone));
                return document.createTextNode(formatPhoneNumber(finalphone));
            } else {
                var el = document.createElement('a');
                el.setAttribute('href', 'tel:' + formatPhoneDashes(finalphone));
                el.setAttribute('style', 'color:inherit');
                el.appendChild(document.createTextNode( formatPhoneNumber(finalphone) ));
            }

            return el;
        }, 0);
    }
};
updatePhoneNumber();